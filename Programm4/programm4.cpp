#include <iostream>
using namespace std;

int main() {
	int value, count, array[33];

	while (true) {
		count = 0;
		cout << "Enter value: ";
		cin >> value;
	
		if (value >= 0) {
			while (value != NULL) {
				array[count] = value & 1;
				value >>= 1;
				count++;
			}
			array[count] = 0;
		}
		else {
			value = abs(value);
			while (value != NULL) {
				array[count] = value & 1;
				value >>= 1;
				count++;
			}
			array[count] = 1;
		}

		for (; count > -1; --count)
			cout << array[count];
		cout << "\n";
	}

	system("pause");
	return 0;
}